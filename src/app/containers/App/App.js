import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import NavigationBar from '../../components/NavigationBar/NavigationBar';
import HomePage from '../HomePage/HomePage';
import BlogsPage from '../BlogsPage/BlogsPage';
import NewPostPage from '../NewPostPage/NewPostPage';
import PostPage from '../PostPage/PostPage';

const App = () => {
    return (
        <Router>
            <NavigationBar />
            <Redirect
                to='/home'
            />
            <Switch>
                <Route path='/home' component={HomePage} />
                <Route path='/blogs' component={BlogsPage} />
                <Route path='/new-post' component={NewPostPage} />
                <Route path='/post/:id' component={PostPage} />
            </Switch>
        </Router>
    );
}

export default App;