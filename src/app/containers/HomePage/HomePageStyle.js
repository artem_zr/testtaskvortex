import styled from 'styled-components';

export const HomePageStyle = styled.div `
    .home-container {
        height: calc(100vh - 56px);
        display: block;
        text-align: center;
        padding-top: 30px;
        background-color: #ededed;
        border: none;
        letter-spacing: 1px;
    }

    .image {
        max-width: 700px;
    }

    .title-text {
        margin: 0 auto;
        width: 60%;
        font-size: 25px;
        font-weight: 500;
        border: 1px solid #707070;
        border-top: none;
    }

    .first-description-text {
        margin: 0 auto;
        width: 80%;
        background-color: #e3e3e3;
        font-size: 30px;
        font-weight: 600;
        border: 1px solid #707070;
        border-top: none;
    }

    .second-description-text {
        background-color: #d1d1d1;
        font-size: 35px;
        font-weight: 700;
        border: 1px solid #707070;
        border-top: none;
    }
`