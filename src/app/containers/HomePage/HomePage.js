import React from 'react';
import { Card } from 'react-bootstrap';

import textData from '../../const/textData/textData.json';
import backgroundImg from '../../../assets/img/background-img-home.jpg';
import {HomePageStyle} from './HomePageStyle';

const HomePage = () => {
    return (
        <HomePageStyle>
            <Card className="home-container">
                <Card.Img src={backgroundImg} className='image' />
                <Card.Body className='title-text'>{textData.TITLE_TEXT_HOME}</Card.Body>
                <Card.Body className='first-description-text'>{textData.FIRST_DESCRIPTION_TEXT_HOME}</Card.Body>
                <Card.Body className='second-description-text'>{textData.SECOND_DESCRIPTION_TEXT_HOME}</Card.Body>
            </Card>
        </HomePageStyle>
    );
};

export default HomePage;