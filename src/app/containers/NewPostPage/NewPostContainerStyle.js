import styled from 'styled-components';

export const NewPostContainerStyle = styled.div `
    .main-container {
        background-color: #e8e8e8;
        max-width: 700px;
        margin: 40px auto 0;
        padding: 30px;
        text-align: center;
        border: 2px solid #707070;
        border-radius: 15px;
        color: #363636;
    }

    .title-text {
        width: 100%;
        font-size: 26px;
        font-weight: 800;
        border-bottom: 2px solid #707070;
        letter-spacing: 2px;
    }

    .description-text {
        max-height: 100px;
        overflow: hidden;
        padding: 5px;
        text-overflow: ellipsis;
    }

    .input-field {
        display: block;
        margin: 30px 0 40px;
    }

    .label-field {
        width: 100%;
        font-size: 20px;
        border: 1px solid #b8b8b8;
        border-top: none;
        border-left: none;
        border-right: none;
        margin-bottom: 15px;
        letter-spacing: 1px;
        padding-bottom: 10px;
    }

    .input-text-area {
        height: 200px;
    }

    .button {
        font-size: 20px;
        font-weight: 500;
        width: 250px;
    }
`