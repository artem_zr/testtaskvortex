import React, { useState } from 'react';

import {Form, Button} from 'react-bootstrap';
import {NewPostContainerStyle} from './NewPostContainerStyle';
import textData from '../../const/textData/textData.json';

const NewPostPage = () => {
    const [postText, setPostText] = useState(null);
    const [postImage, setPostImage] = useState(null);

    const postImageChange = (e) => {
        const imagePath = e.target.value;

        setPostImage(imagePath);
    };

    const postTextChange = (e) => {
        const textData = e.target.value;

        setPostText(textData);
    };

    const createNewPostClick = () => {
        console.log(`Post Text: ${postText}\n Image Path: ${postImage}`);
    };

    return (
        <NewPostContainerStyle>
            <Form className='main-container'>
                <Form.Label className='title-text'>{textData.TITLE_TEXT_NEW_PAGE}</Form.Label>
                <Form.Group className='input-field'>
                    <Form.Label className='label-field'>{textData.LABEL_TEXT_INPUT_FILE_NEW_POST}</Form.Label>
                    <Form.File onChange={postImageChange} />
                </Form.Group>
                <Form.Group className='input-field'>
                    <Form.Label className='label-field'>{textData.LABEL_TEXT_AREA_NEW_POST}</Form.Label>
                    <Form.Control as="textarea" className='input-text-area' onChange={postTextChange} />
                </Form.Group>
                <Button variant="outline-secondary" className='button' onClick={createNewPostClick}>
                    {textData.BUTTON_TEXT_NEW_POST}
                </Button>
            </Form>
        </NewPostContainerStyle>
    );
};

export default NewPostPage;