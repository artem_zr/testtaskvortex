import React from 'react';

import PostCard from '../../components/PostCard/PostCard';
import postCardsData from '../../const/postCardsData/postCardsData';

const PostPage = (props) => {
    const id = props.match.params.id;
    const cardData = postCardsData.find(card => card.id == id);
    const {image, titleText, descriptionText} = cardData || {};

    return (
        <PostCard 
            image={image}
            titleText={titleText}
            descriptionText={descriptionText}
        />
    );
};

export default PostPage;