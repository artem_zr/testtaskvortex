import React from 'react';
import { Row, Container } from 'react-bootstrap';

import BlogsPostCard from '../../components/PostCard/BlogsPostCard/BlogsPostCard';
import postCardsData from '../../const/postCardsData/postCardsData';

const BlogsPage = () => {
    return (
        <Container fluid>
            <Row className='justify-content-md-center'>
                {postCardsData.map((item, index) => {
                    const id = item.id;
                    const image = item.image;
                    const titleText = item.titleText;
                    const descriptionText = item.descriptionText;
                    
                    return (
                        <BlogsPostCard 
                            id={id}
                            image={image}
                            titleText={titleText}
                            descriptionText={descriptionText}
                            key={index.toString()}
                        />
                    );
                })}
            </Row>
        </Container>
    );
};

export default BlogsPage;