import textData from '../textData/textData.json';
import cardImg1 from '../../../assets/img/card-img-1.jpg';
import cardImg2 from '../../../assets/img/card-img-2.jpg';
import cardImg3 from '../../../assets/img/card-img-3.jpg';
import cardImg4 from '../../../assets/img/card-img-4.jpg';

export default [
    {
        id: 1,
        image: cardImg1,
        titleText: textData.TITLE_TEXT_FIRST_POST,
        descriptionText: textData.DESCRIPTION_TEXT_FIRST_POST,
    },
    {
        id: 2,
        image: cardImg2,
        titleText: textData.TITLE_TEXT_SECOND_POST,
        descriptionText: textData.DESCRIPTION_TEXT_SECOND_POST,
    },
    {
        id: 3,
        image: cardImg3,
        titleText: textData.TITLE_TEXT_THIRD_POST,
        descriptionText: textData.DESCRIPTION_TEXT_THIRD_POST,
    },
    {
        id: 4,
        image: cardImg4,
        titleText: textData.TITLE_TEXT_FOURTH_POST,
        descriptionText: textData.DESCRIPTION_TEXT_FOURTH_POST,
    }
];