import styled from 'styled-components';

export const PostCardStyle = styled.div `
    .card-container {
        
        background-color: #ededed;
        max-width: 800px; 
        margin: 30px auto;
    }

    .description-text {
        max-height: 700px;
        padding: 5px;
    }

    .button {
        margin: 0 auto;
        color: #ededed;
    }
`