import React from 'react';
import { Card, Button, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';

import textData from '../../../const/textData/textData.json';
import {BlogsPostStyleContainer} from './BlogsPostCardStyle.js';

const BlogsPostCard = (props) => {
    const {id, image, titleText, descriptionText} = props;

    return (
        <BlogsPostStyleContainer>
            <Col>
                <Card className={'main-container'}>
                    <Card.Img variant='top' src={image} />
                    <Card.Body>
                        <Card.Title>{titleText}</Card.Title>
                        <Card.Text className='description-text'>{descriptionText}</Card.Text>
                        <Button variant='primary'>
                            <Link className='button' to={`/post/${id}`}>{textData.BUTTON_TEXT_POST_CARD}</Link>
                        </Button>
                    </Card.Body>
                </Card>
            </Col>
        </BlogsPostStyleContainer>
    );
};

export default BlogsPostCard;