import styled from 'styled-components';

export const BlogsPostStyleContainer = styled.div `
    .main-container {
        background-color: #e8e8e8;
        max-width: 500px; 
        margin: 20px;

        &:hover {
            background-color: #d6d6d6;
        }
    }

    .description-text {
        max-height: 100px;
        padding: 5px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .button {
        color: #ffffff;
    }
`