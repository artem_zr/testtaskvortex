import React from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

import textData from '../../const/textData/textData.json';
import {PostCardStyle} from './PostCardStyle';

const PostCard = (props) => {
    const {image, titleText, descriptionText} = props;

    return (
        <PostCardStyle>
            <Card className='card-container'>
                <Card.Img variant='top' src={image} />
                <Card.Body>
                    <Card.Title>{titleText}</Card.Title>
                    <Card.Text className='description-text'>{descriptionText}</Card.Text>
                    <Button variant='secondary'>
                        <Link className='button' to='/blogs'>{textData.BUTTON_TEXT_POST_CARD_BACK}</Link>
                    </Button>
                </Card.Body>
            </Card>
        </PostCardStyle>
    );
};

export default PostCard;