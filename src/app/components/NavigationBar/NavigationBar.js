import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';

import textData from '../../const/textData/textData.json';
import {NavigationBarStyle} from './NavigationBarStyle.js';

const NavigationBar = () => {
    return (
        <NavigationBarStyle>
            <Navbar collapseOnSelect expand='lg' bg='dark' variant='dark'>
                <Navbar.Brand>{textData.HEADER_TEXT_NAVIGATION}</Navbar.Brand>
                <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                <Navbar.Collapse id='responsive-navbar-nav'>
                    <Nav className='mr-auto'>
                        <Nav.Item className='nav-item'><Link to='/home'>{textData.HOME_PAGE_LABEL_NAVIGATION}</Link></Nav.Item>
                        <Nav.Item className='nav-item'><Link to='/blogs'>{textData.BLOGS_PAGE_LABEL_NAVIGATION}</Link></Nav.Item>
                        <Nav.Item className='nav-item'><Link to='/new-post'>{textData.NEW_POST_PAGE_LABEL_NAVIGATION}</Link></Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </NavigationBarStyle>
    );
};

export default NavigationBar;