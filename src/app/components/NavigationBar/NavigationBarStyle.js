import styled from 'styled-components';

export const NavigationBarStyle = styled.div `
    a, .navbar-brand, .navbar-nav, .nav-link {
        color: #ebebeb;

        &:hover {
            color: #d6d6d6;
        }
    }

    .nav-item {
        margin-right: 30px;
    }
`